<?php

namespace Drupal\webform_widget_handler_example\Plugin\WebformHandler;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\Utility\WebformYaml;

/**
 * Webform handler.
 *
 * @WebformHandler(
 *   id = "webform_widget_handler_example",
 *   label = @Translation("Example widget handler"),
 *   category = @Translation("Field widget handler"),
 *   description = @Translation("Allows for the selection of a variant."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 * )
 */
class ExampleWidgetHandler extends WebformHandlerBase {

  /**
   * Provide form elements to display on the widget.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   Array of default values for this field.
   * @param int $delta
   *   The order of this item in the array of sub-elements (0, 1, 2, etc.).
   * @param array $element
   *   A form element array containing basic properties for the widget:
   *   - #field_parents: The 'parents' space for the field in the form. Most
   *       widgets can simply overlook this property. This identifies the
   *       location where the field values are placed within
   *       $form_state->getValues(), and is used to access processing
   *       information for the field through the getWidgetState() and
   *       setWidgetState() methods.
   *   - #title: The sanitized element label for the field, ready for output.
   *   - #description: The sanitized element description for the field, ready
   *     for output.
   *   - #required: A Boolean indicating whether the element value is required;
   *     for required multiple value fields, only the first widget's values are
   *     required.
   *   - #delta: The order of this item in the array of sub-elements; see $delta
   *     above.
   * @param array $form
   *   The form structure where widgets are being attached to. This might be a
   *   full form structure, or a sub-element of a larger form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The current state of the form.
   *
   * @return array
   *   The form elements for a single widget for this field.
   */
  public function formElementFieldWidget(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $formState) : array {
    $defaultData = WebformYaml::decode($items[$delta]->default_data);

    $element['settings']['status_message']['variant'] = [
      '#type' => 'select',
      '#title' => $this->t('Variant'),
      '#default_value' => $defaultData['variant'] ?? '',
      '#options' => [
        '' => $this->t('Default'),
        'without_name' => $this->t('Without name'),
        'without_name_nor_subject' => $this->t('Without name nor subject'),
      ],
    ];

    $element['settings']['default_data']['#process'] = [
      [get_called_class(), 'elementProcessCallback'],
    ];

    return $element;
  }

  /**
   * Element process callback.
   *
   * @param array $element
   *   Form element.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state.
   */
  public static function elementProcessCallback(array $element, FormStateInterface $formState) : array {
    $defaultData = WebformYaml::decode($formState->getValue($element['#parents']) ?? '{}');

    $valueKey = array_merge(
      array_slice($element['#parents'], 0, -1),
      ['status_message', 'variant']
    );
    $defaultData['variant'] = $formState->getValue($valueKey);
    $formState->setValue($element['#parents'], WebformYaml::encode($defaultData));

    return $element;
  }

}
