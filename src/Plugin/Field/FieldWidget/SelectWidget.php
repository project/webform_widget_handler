<?php

namespace Drupal\webform_widget_handler\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Entity\Webform;
use Drupal\webform\Plugin\Field\FieldWidget\WebformEntityReferenceSelectWidget;

/**
 * Defines the 'webform_widget_handler_select' field widget.
 *
 * @FieldWidget(
 *   id = "webform_widget_handler_select",
 *   label = @Translation("Select list with handler control"),
 *   field_types = {"webform"},
 * )
 */
class SelectWidget extends WebformEntityReferenceSelectWidget {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'status_enabled' => TRUE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);

    $element['status_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable status configuration'),
      '#default_value' => $this->getSetting('status_enabled'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $summary[] = $this->t('Status configuration: @status', ['@status' => $this->getSetting('status_enabled') ? $this->t('Yes') : $this->t('No')]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    if (!$this->getSetting('status_enabled')) {
      $element['settings']['status']['#access'] = FALSE;
      $element['settings']['scheduled']['#access'] = FALSE;
    }

    // Get field name.
    $field_name = $items->getName();

    // Get webform.
    $target_id = NULL;
    if ($form_state->isRebuilding()) {
      $target_id = $form_state->getValue(array_merge(
        $element['target_id']['#field_parents'],
        [$field_name, $delta, 'target_id']
      ));
    }
    if (!$target_id) {
      $target_id = $items[$delta]->target_id;
    }

    // Let handlers alter the form.
    /** @var \Drupal\webform\WebformInterface $webform */
    $webform = ($target_id) ? Webform::load($target_id) : NULL;
    if (!$webform) {
      return $element;
    }
    $handlers = $webform->getHandlers();
    foreach ($handlers as $handler) {
      if (method_exists($handler, 'formElementFieldWidget')) {
        $element = $handler->formElementFieldWidget($items, $delta, $element, $form, $form_state);
      }
    }

    return $element;
  }

}
