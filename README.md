# Webform widget handler

Allows for webform handlers to alter the field widget editors see
when a webform is embedded into an entity with a field.

For example instead of presenting a YAML field to provide
default data for the webform you can replace that YAML
with a select field that has webform variants as options
so the editor can see a summary of what the variant does.

## Setup

First a webform custom handler is needed.
This handler must implement a `formElementFieldWidget` method.
Add an instance of that handler to a webform. There is an example
in the `modules` folder of this project that shows a select form field
to choose between two variants.

Add a webform field to you entity and configure the field
widget that this module provides: "Select list with handler control".
It is recommended to uncheck "Enable default submission data (YAML)"
hiding the textarea to write YAML.

Add or edit an entity that has the webform field configured,
select a webform that has the custom handler enabled and
the custom handler will be called to alter the widget.

## Example module

The example module shipped along with the main module
showcases how to use with a contact webform similar to
the one from the webform module adding two variants that
remove the name or the subject of the form when they are active.

The variants can be activated from the widget that embeds the form.
Note that the webform field is not created with the example module,
just the webform so you need to add it manually and configure
the form display mode.

## Future plans

Instead of having to write a Drupal form logic use another webform to
display in the widget and save the submission as "default_values" for
the webform that will be embedded.
